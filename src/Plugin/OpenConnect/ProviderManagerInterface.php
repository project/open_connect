<?php

namespace Drupal\open_connect\Plugin\OpenConnect;

use Drupal\Component\Plugin\PluginManagerInterface;

interface ProviderManagerInterface extends PluginManagerInterface {

}
